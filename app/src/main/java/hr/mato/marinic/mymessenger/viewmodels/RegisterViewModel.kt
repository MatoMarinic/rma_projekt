package hr.mato.marinic.mymessenger.viewmodels


import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import hr.mato.marinic.mymessenger.repository.remote_data_source.auth.AuthSource
import hr.mato.marinic.mymessenger.utils.State
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class RegisterViewModel(
    private val authSource: AuthSource
) : ViewModel() {

    private val onRegister: MediatorLiveData<State> = MediatorLiveData()
    private val disposable = CompositeDisposable()


    fun register(email: String, password: String, name: String) {
        authSource.register(email, password, name)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {

                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                    onRegister.value = State.LOADING
                }

                override fun onComplete() {
                    onRegister.value = State.SUCCESS
                }

                override fun onError(e: Throwable) {
                    onRegister.value = State.ERROR
                }
            })
    }

    fun observeRegister(): LiveData<State> {
        return onRegister
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}