package hr.mato.marinic.mymessenger.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.mato.marinic.mymessenger.model.Chat
import hr.mato.marinic.mymessenger.model.User
import hr.mato.marinic.mymessenger.repository.Repository
import hr.mato.marinic.mymessenger.utils.State
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ChatMenuViewModel(
    private val repository: Repository
) : ViewModel() {

    private val disposable = CompositeDisposable()
    private val onQuery: MediatorLiveData<State> = MediatorLiveData()
    private val chatList: MutableLiveData<List<Chat>> = MutableLiveData()
    private val user: MutableLiveData<User> = MutableLiveData()


    fun getUser(): LiveData<User> {
        if (user.value == null) {
            loadUser()
        }
        return user
    }

    private fun loadUser() {
        repository.getCurrentUser()!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .toObservable()
            .subscribe(object : Observer<User> {
                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                    onQuery.value = State.LOADING
                }

                override fun onNext(t: User) {
                    user.value = t
                }

                override fun onError(e: Throwable) {
                    onQuery.value = State.ERROR
                }

                override fun onComplete() {
                    onQuery.value = State.SUCCESS
                    loadChats()
                }
            })
    }

    fun getChats(): LiveData<List<Chat>> {
        if (chatList.value == null) {
            loadChats()
        }
        return chatList
    }

    fun loadChats() {
        repository.getChatList(user.value?.userId ?: "")!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .toObservable()
            .subscribe(object : Observer<List<Chat>> {
                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                    onQuery.value = State.LOADING
                }

                override fun onNext(t: List<Chat>) {
                    chatList.value =
                        t.filter { it.messages.isNotEmpty() }.sortedByDescending { it.messages.last().date }
                }

                override fun onError(e: Throwable) {
                    onQuery.value = State.ERROR
                }

                override fun onComplete() {
                    onQuery.value = State.SUCCESS
                }
            })
    }

    fun observeQuery(): LiveData<State> {
        return onQuery
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}