package hr.mato.marinic.mymessenger.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.mato.marinic.mymessenger.model.User
import hr.mato.marinic.mymessenger.repository.Repository
import hr.mato.marinic.mymessenger.repository.remote_data_source.auth.AuthSource
import hr.mato.marinic.mymessenger.utils.State
import io.reactivex.CompletableObserver
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class LoginViewModel(
    private val authSource: AuthSource,
    private val repository: Repository
) : ViewModel() {

    private val onLogin: MediatorLiveData<State> = MediatorLiveData()
    private val disposable = CompositeDisposable()
    private val user: MutableLiveData<User> = MutableLiveData()



    fun getUser(): LiveData<User> {
        if (user.value == null) {
            loadUser()
        }
        return user
    }

    private fun loadUser() {
        repository.getCurrentUser()!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .toObservable()
            .subscribe(object : Observer<User> {
                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                }

                override fun onNext(t: User) {
                    storeFcmToken(t.userId)
                    user.value = t
                }

                override fun onError(e: Throwable) {
                }

                override fun onComplete() {}
            })

    }

    private fun storeFcmToken(userId: String) {
        repository.storeFcmToken(userId)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                }

                override fun onComplete() {
                    if (onLogin.value == State.LOADING) {
                        onLogin.value = State.SUCCESS
                    }
                }

                override fun onError(e: Throwable) {
                    onLogin.value = State.ERROR
                }
            })
    }

    fun login(email: String?, password: String?) {
        authSource.login(email, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                    onLogin.value = State.LOADING
                }

                override fun onComplete() {

                    loadUser()
                }

                override fun onError(e: Throwable) {
                    onLogin.value = State.ERROR
                }
            })
    }

    fun observeLogin(): LiveData<State> {
        return onLogin
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}