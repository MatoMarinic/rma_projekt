package hr.mato.marinic.mymessenger.di

import androidx.room.Room
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import hr.mato.marinic.mymessenger.repository.Repository
import hr.mato.marinic.mymessenger.repository.RepositoryImpl
import hr.mato.marinic.mymessenger.repository.local_database.MyMessengerDatabase
import hr.mato.marinic.mymessenger.repository.local_database.RoomRepository
import hr.mato.marinic.mymessenger.repository.local_database.RoomRepositoryImpl
import hr.mato.marinic.mymessenger.repository.remote_data_source.auth.AuthSource
import hr.mato.marinic.mymessenger.repository.remote_data_source.auth.AuthSourceImpl
import hr.mato.marinic.mymessenger.repository.remote_data_source.database.FirestoreSource
import hr.mato.marinic.mymessenger.repository.remote_data_source.database.FirestoreSourceImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val repoModule = module {

    single {
        Room.databaseBuilder(
            androidApplication(),
            MyMessengerDatabase::class.java,
            "MessengerDatabase"
        ).build()
    }

    single {
        FirebaseAuth.getInstance()
    }

    single {
        FirebaseFirestore.getInstance()
    }

    single {
        FirebaseStorage.getInstance()
    }

    single<AuthSource> { AuthSourceImpl(get(), get()) }

    single<FirestoreSource> { FirestoreSourceImpl(get(), get(), get()) }

    single<RoomRepository> { RoomRepositoryImpl(get()) }

    single<Repository> { RepositoryImpl(get(), get()) }

}