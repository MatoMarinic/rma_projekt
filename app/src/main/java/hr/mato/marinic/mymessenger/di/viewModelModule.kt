package hr.mato.marinic.mymessenger.di


import hr.mato.marinic.mymessenger.viewmodels.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { RegisterViewModel(get()) }

    viewModel { LoginViewModel(get(), get()) }

    viewModel { ChatMenuViewModel(get()) }

    viewModel { AddFriendsViewModel(get()) }

    viewModel { ChatViewModel(get()) }
}