package hr.mato.marinic.mymessenger.repository

import hr.mato.marinic.mymessenger.model.Chat
import hr.mato.marinic.mymessenger.model.User
import io.reactivex.Completable
import io.reactivex.Single

interface Repository {

    fun getChatList(userId: String): Single<List<Chat>>?

    fun getUser(userId: String): Single<User>?

    fun getAllUsers(): Single<List<User>>?

    fun getCurrentUser(): Single<User>?

    fun uploadImage(imageBytes: ByteArray): Single<String>?

    fun createChat(userId1: String, userId2: String): Completable?

    fun updateChat(chat: Chat): Completable?

    fun getChat(chatId: String): Single<Chat>?

    fun storeFcmToken(userId: String): Completable?
}