package hr.mato.marinic.mymessenger.repository.local_database.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import hr.mato.marinic.mymessenger.model.Message
import java.util.*


class TypeConverter {
    companion object {
        @TypeConverter
        @JvmStatic
        fun listToJson(value: List<Message>?): String = Gson().toJson(value)

        @TypeConverter
        @JvmStatic
        fun jsonToList(value: String) = Gson().fromJson(value, Array<Message>::class.java).toList()
    }
}