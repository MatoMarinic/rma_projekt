package hr.mato.marinic.mymessenger.repository.remote_data_source.database

import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import hr.mato.marinic.mymessenger.model.Chat
import hr.mato.marinic.mymessenger.model.User
import hr.mato.marinic.mymessenger.utils.Constants.CHATS_COLLECTION
import hr.mato.marinic.mymessenger.utils.Constants.USERS_COLLECTION
import io.reactivex.Completable
import io.reactivex.Single
import java.util.*


class FirestoreSourceImpl(
    private val firebaseFirestore: FirebaseFirestore,
    private val firebaseAuth: FirebaseAuth,
    private val firebaseStorage: FirebaseStorage
) : FirestoreSource {

    override fun getUserUUID(): String = firebaseAuth.currentUser!!.uid


    override fun getChatList(userId: String): Single<List<Chat>> {
        return Single.create {
            firebaseFirestore
                .collection(CHATS_COLLECTION)
                .whereArrayContains("users", userId)
                .orderBy("lastUpdateTimestamp", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener { task ->

                    if (task.isSuccessful && task.result != null) {
                        val chatList: List<Chat> =
                            task.result!!.toObjects(Chat::class.java)
                        it.onSuccess(chatList)
                    }
                }
        }
    }

    override fun getCurrentUser(): Single<User> {
        return Single.create {
            firebaseFirestore
                .collection(USERS_COLLECTION)
                .document(getUserUUID())
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        val user: User = task.result?.toObject(User::class.java) as User
                        it.onSuccess(user)
                    }
                }
        }
    }

    override fun getUser(userId: String): Single<User> {
        return Single.create {
            firebaseFirestore
                .collection(USERS_COLLECTION)
                .document(userId)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        val user: User = task.result?.toObject(User::class.java) as User
                        it.onSuccess(user)
                    }
                }
        }
    }

    override fun getAllUsers(): Single<List<User>>? {
        return Single.create {
            firebaseFirestore
                .collection(USERS_COLLECTION)
                .orderBy("nickname", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        val allUsers: List<User> =
                            task.result!!.toObjects(User::class.java)
                        it.onSuccess(allUsers)
                    }
                }
        }
    }

    override fun uploadImage(image: ByteArray): Single<String> {
        return Single.create { e ->
            val storageReference: StorageReference =
                firebaseStorage.reference
                    .child(
                        "images/" + getUserUUID().toString() + "/" + UUID.randomUUID().toString()
                    )
            storageReference
                .putBytes(image)
                .addOnSuccessListener {
                    storageReference.downloadUrl
                        .addOnSuccessListener { uri -> e.onSuccess(uri.toString()) }
                        .addOnFailureListener { error -> e.onError(error) }

                }
        }
    }

    override fun createChat(userId1: String, userId2: String): Completable {
        val userRef = firebaseFirestore.collection(USERS_COLLECTION).document(getUserUUID())
        val user2Ref = firebaseFirestore.collection(USERS_COLLECTION).document(userId2)
        val chatRef =
            firebaseFirestore.collection(CHATS_COLLECTION).document("${userId1}${userId2}")

        return Completable.create { e ->

            firebaseFirestore
                .collection(USERS_COLLECTION)
                .whereIn("userId", listOf(userId1, userId2))
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {

                        val users = task.result?.toObjects(User::class.java) as List<User>
                        val userNicknames = users.map { it.nickname }
                        firebaseFirestore.runBatch { batch ->
                            batch.set(
                                chatRef,
                                Chat(
                                    id = "${userId1}${userId2}",
                                    listOf(userId1, userId2),
                                    userNicknames,
                                    listOf()
                                )
                            )

                            batch.update(
                                userRef,
                                "chats",
                                FieldValue.arrayUnion("${userId1}${userId2}")
                            )
                            batch.update(
                                user2Ref,
                                "chats",
                                FieldValue.arrayUnion("${userId1}${userId2}")
                            )


                        }
                            .addOnCompleteListener { e.onComplete() }
                            .addOnFailureListener { error -> e.onError(error) }
                    }
                }


        }
    }

    override fun updateChat(chat: Chat): Completable {

        return Completable.create { e ->
            firebaseFirestore
                .collection(CHATS_COLLECTION)
                .document(chat.id)
                .set(chat)
                .addOnCompleteListener { e.onComplete() }
                .addOnFailureListener { error -> e.onError(error) }
        }
    }

    override fun getChat(chatId: String): Single<Chat> {
        return Single.create {
            firebaseFirestore
                .collection(CHATS_COLLECTION)
                .document(chatId)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result.data != null) {

                        val chat: Chat = task.result?.toObject(Chat::class.java) as Chat
                        it.onSuccess(chat)
                    }
                }
        }
    }

    override fun storeFcmToken(userId: String): Completable {
        return Completable.create { e ->
            FirebaseMessaging.getInstance().token
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        return@OnCompleteListener
                    }

                    val token = task.result

                    firebaseFirestore
                        .collection(USERS_COLLECTION)
                        .document(userId)
                        .update("fcmToken", token)
                        .addOnCompleteListener { e.onComplete() }
                        .addOnFailureListener { error -> e.onError(error) }
                })
        }
    }
}