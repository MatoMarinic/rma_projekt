package hr.mato.marinic.mymessenger.repository.local_database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import hr.mato.marinic.mymessenger.model.Chat

@Dao
interface ChatDao {

    @Insert(onConflict = REPLACE)
    fun addNewChat(imagePost: Chat)

    @Query("SELECT * FROM Chat WHERE id = :id")
    fun getSingleChat(id: String): Chat
}