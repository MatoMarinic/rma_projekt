package hr.mato.marinic.mymessenger.ui.chat_screen.recycler_adapter

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import hr.mato.marinic.mymessenger.databinding.MessageItemOutBinding
import hr.mato.marinic.mymessenger.model.Message
import hr.mato.marinic.mymessenger.utils.StringUtils

class OutMessageViewHolder(private val messageItemOutBinding: MessageItemOutBinding) :
    RecyclerView.ViewHolder(messageItemOutBinding.root) {

    fun onBind(messageList: List<Message>, position: Int) {
        if (position != 0 && messageList[position - 1].userId == messageList[position].userId) {
            messageItemOutBinding.chatNicknameOut.visibility = View.GONE
        } else {
            messageItemOutBinding.chatNicknameOut.visibility = View.VISIBLE
        }

        if (messageList[position] == messageList.last()) {
            messageItemOutBinding.chatDateOut.visibility = View.VISIBLE
        } else if (position == 0 && messageList[0].userId == messageList[position + 1].userId) {
            messageItemOutBinding.chatDateOut.visibility = View.GONE
        } else if (position == 0 && messageList[0].userId != messageList[position + 1].userId) {
            messageItemOutBinding.chatDateOut.visibility = View.VISIBLE
        } else if (position < messageList.size && messageList[position].userId != messageList[position + 1].userId) {
            messageItemOutBinding.chatDateOut.visibility = View.VISIBLE
        } else if (messageList[position - 1].userId != messageList[position].userId) {
            messageItemOutBinding.chatDateOut.visibility = View.GONE
        } else if (position < messageList.size && messageList[position].userId != messageList[position + 1].userId) {
            messageItemOutBinding.chatDateOut.visibility = View.VISIBLE
        } else if (messageList[position - 1].userId == messageList[position].userId) {
            messageItemOutBinding.chatDateOut.visibility = View.GONE
        } else {
            messageItemOutBinding.chatDateOut.visibility = View.VISIBLE
        }

        if (messageList[position].content.startsWith("https://firebasestorage.googleapis.com/")) {
            messageItemOutBinding.imageMessageOut.visibility = View.VISIBLE
            messageItemOutBinding.chatMessageOut.visibility = View.GONE
            messageItemOutBinding.imageUrl = messageList[position].content
        } else {
            messageItemOutBinding.imageMessageOut.visibility = View.GONE
            messageItemOutBinding.chatMessageOut.visibility = View.VISIBLE
        }


        messageItemOutBinding.message = messageList[position]
        messageItemOutBinding.stringUtils = StringUtils
        messageItemOutBinding.executePendingBindings()
    }

    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ImageView, url: String?) {
            if (!url.isNullOrEmpty()) {
                Glide.with(view.context).load(url).into(view)
            }
        }
    }
}