package hr.mato.marinic.mymessenger.repository.local_database

import hr.mato.marinic.mymessenger.model.Chat
import hr.mato.marinic.mymessenger.model.User

interface RoomRepository {

    fun addNewChat(chat: Chat)

    fun addNewUser(user: User)

    fun getSingleChat(id: String): Chat
}