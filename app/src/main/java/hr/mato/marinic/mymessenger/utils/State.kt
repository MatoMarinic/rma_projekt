package hr.mato.marinic.mymessenger.utils

enum class State { SUCCESS, ERROR, LOADING }
