package hr.mato.marinic.mymessenger.utils

interface BooleanListener {
    fun onYes()
}