package hr.mato.marinic.mymessenger.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey
    var userId: String = "0",
    var email: String = "unknown@unknown.unknown",
    var nickname: String = "Unknown",
    var fcmToken: String = "",

    @Ignore
    var chats: List<String> = listOf()
)