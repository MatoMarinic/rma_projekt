package hr.mato.marinic.mymessenger.repository.remote_data_source.database

import hr.mato.marinic.mymessenger.model.Chat
import hr.mato.marinic.mymessenger.model.User
import io.reactivex.Completable
import io.reactivex.Single

interface FirestoreSource {

    fun getUserUUID(): String

    fun getChatList(userId: String): Single<List<Chat>>?

    fun getCurrentUser(): Single<User>?

    fun getUser(userId: String): Single<User>?

    fun getAllUsers(): Single<List<User>>?

    fun uploadImage(image: ByteArray): Single<String>?

    fun createChat(userId1: String, userId2: String): Completable?

    fun updateChat(chat: Chat): Completable?

    fun getChat(chatId: String): Single<Chat>?

    fun storeFcmToken(userId: String): Completable?
}