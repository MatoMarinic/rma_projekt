package hr.mato.marinic.mymessenger.utils

object Constants {

    //firestore collections
    const val USERS_COLLECTION = "users"
    const val CHATS_COLLECTION = "chats"
    const val IMAGES_COLLECTION = "images"

    //shared prefs
    const val LOGIN = "LOGIN"
    const val IS_LOGGED_IN = "IS_LOGGED_IN"

    // keys for key-value pairs
    const val USER_ID_SHARED = "USER_ID"
    const val USER_ID_EXTRA = "USER_ID_EXTRA"
    const val CHAT_ID_EXTRA = "CHAT_ID_EXTRA"
    const val ADD_INFO_EXTRA = "ADD_INFO_EXTRA"

    const val PICK_IMAGE_REQUEST = 71

    // 2 different chat message layouts
    const val TYPE_OUT_MESSAGE = 1
    const val TYPE_INC_MESSAGE = 2

    // time measures for date
    const val MINUTE = 60
    const val TWO_MINUTES = 120
    const val HOUR = 3_600
    const val TWO_HOURS = 7_200
    const val DAY = 86_400
    const val TWO_DAYS = 172_800
    const val FORTY_DAYS = 3_456_000
}