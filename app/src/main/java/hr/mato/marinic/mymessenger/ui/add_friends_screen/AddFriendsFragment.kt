package hr.mato.marinic.mymessenger.ui.add_friends_screen

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.core.view.get
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import hr.mato.marinic.mymessenger.R
import hr.mato.marinic.mymessenger.databinding.FragmentAddFriendBinding
import hr.mato.marinic.mymessenger.ui.add_friends_screen.recycler_adapter.AddFriendRecyclerAdapter
import hr.mato.marinic.mymessenger.ui.base_fragment.BaseBottomTabFragment
import hr.mato.marinic.mymessenger.utils.Constants
import hr.mato.marinic.mymessenger.utils.Constants.USER_ID_EXTRA
import hr.mato.marinic.mymessenger.utils.State
import hr.mato.marinic.mymessenger.utils.hide
import hr.mato.marinic.mymessenger.utils.show
import hr.mato.marinic.mymessenger.viewmodels.AddFriendsViewModel
import kotlinx.android.synthetic.main.fragment_add_friend.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddFriendsFragment : BaseBottomTabFragment() {

    private val viewModel by viewModel<AddFriendsViewModel>()
    private lateinit var navController: NavController
    private val adapter by lazy {
        AddFriendRecyclerAdapter(
            getUserId()!!
        ) { onSelected(it) }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAddFriendBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_add_friend, container, false)
        binding.profileRecyclerView.adapter = adapter
        return binding.root
    }

    private fun getUserId(): String? {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        return sharedPref?.getString(Constants.USER_ID_SHARED, "")
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navController = findNavController()

        addFriendToolbar.navigationIcon = resources.getDrawable(
            R.drawable.ic_baseline_arrow_back_24,
            null
        )
        addFriendToolbar.setNavigationOnClickListener { navController.popBackStack() }
        observeData()
        subscribeObservers()
        initRecycler()
        setSearchView()
    }

    private fun initRecycler() {
        profileRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    private fun subscribeObservers() {
        viewModel.observeQuery().observe(viewLifecycleOwner, Observer<State> {
            if (it != null) {
                when (it) {
                    State.LOADING -> show(profileProgress)
                    State.SUCCESS -> hide(profileProgress)
                    State.ERROR -> hide(profileProgress)
                }
            }
        })
    }

    private fun observeData() {
        viewModel.getAllUsers().removeObservers(this)
        viewModel.getAllUsers().observe(viewLifecycleOwner, Observer { taskList ->
            run {
                adapter.addItems(taskList)
            }
        })

        viewModel.getUser().removeObservers(this)
        viewModel.getUser().observe(viewLifecycleOwner, Observer { user ->
            run {
                addFriendToolbar.title =
                    String.format(
                        requireContext().getString(R.string.add_friends_title),
                        user.nickname
                    )
            }
        })
    }

    private fun onSelected(userId: String) {
        goToChatFragment(userId)
    }

    private fun goToChatFragment(userId: String) {
        navController.navigate(
            R.id.action_addFriendFragment_to_chatFragment,
            bundleOf(Pair(USER_ID_EXTRA, userId))
        )
    }

    private fun setSearchView() {
        val searchView = addFriendToolbar.menu[0].actionView as SearchView
        searchView.imeOptions = EditorInfo.IME_ACTION_SEARCH
        searchView.queryHint = getString(R.string.search_hint)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                adapter.filter.filter(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
        })
    }

}