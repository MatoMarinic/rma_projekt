package hr.mato.marinic.mymessenger

import android.app.Application
import hr.mato.marinic.mymessenger.di.repoModule
import hr.mato.marinic.mymessenger.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.logger.Level

class Messenger : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(level = Level.ERROR)
            androidContext(this@Messenger)
            modules(listOf(repoModule, viewModelModule))
        }
    }
}

