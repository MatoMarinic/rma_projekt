package hr.mato.marinic.mymessenger.repository

import hr.mato.marinic.mymessenger.model.Chat
import hr.mato.marinic.mymessenger.model.User
import hr.mato.marinic.mymessenger.repository.local_database.RoomRepository
import hr.mato.marinic.mymessenger.repository.remote_data_source.database.FirestoreSource
import io.reactivex.Completable
import io.reactivex.Single

class RepositoryImpl(
    private val firestoreSource: FirestoreSource,
    private val roomRepository: RoomRepository
) : Repository {

    override fun getChatList(userId: String): Single<List<Chat>>? =
        firestoreSource.getChatList(userId)

    override fun getCurrentUser(): Single<User>? = firestoreSource.getCurrentUser()


    override fun uploadImage(imageBytes: ByteArray): Single<String>? =
        firestoreSource.uploadImage(imageBytes)


    override fun getUser(userId: String): Single<User>? {
        return firestoreSource.getUser(userId)
    }

    override fun getAllUsers(): Single<List<User>>? {
        return firestoreSource.getAllUsers()
    }

    override fun createChat(userId1: String, userId2: String): Completable? {
        return firestoreSource.createChat(userId1, userId2)
    }

    override fun updateChat(chat: Chat): Completable? {
        return firestoreSource.updateChat(chat)
    }

    override fun getChat(chatId: String): Single<Chat>? {
        return firestoreSource.getChat(chatId)
    }

    override fun storeFcmToken(userId: String): Completable? {
        return firestoreSource.storeFcmToken(userId)
    }
}