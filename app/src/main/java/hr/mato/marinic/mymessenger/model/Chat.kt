package hr.mato.marinic.mymessenger.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.firebase.Timestamp
import com.google.firebase.firestore.ServerTimestamp

@Entity
data class Chat(
    @PrimaryKey
    var id: String = "0",
    @Ignore
    var users: List<String> = listOf(),
    @Ignore
    var nicknames: List<String>? = listOf(),

    var messages: List<Message> = listOf(),

    @Ignore
    @ServerTimestamp  var lastUpdateTimestamp: Timestamp? = null
)