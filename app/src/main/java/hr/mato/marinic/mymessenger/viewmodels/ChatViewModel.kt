package hr.mato.marinic.mymessenger.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.mato.marinic.mymessenger.model.Chat
import hr.mato.marinic.mymessenger.model.Message
import hr.mato.marinic.mymessenger.model.User
import hr.mato.marinic.mymessenger.repository.Repository
import hr.mato.marinic.mymessenger.utils.State
import io.reactivex.CompletableObserver
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class ChatViewModel(
    private val repository: Repository
) : ViewModel() {

    private val disposable = CompositeDisposable()
    private val onQuery: MediatorLiveData<State> = MediatorLiveData()
    private val onSend: MediatorLiveData<State> = MediatorLiveData()
    private val currentUser: MutableLiveData<User> = MutableLiveData()

    private val image: MutableLiveData<ByteArray> = MutableLiveData()

    private var userId2: String = ""
    var chatId: String = ""
    private val chat: MutableLiveData<Chat> = MutableLiveData()


    fun setImage(imageBytes: ByteArray) {
        image.value = imageBytes
    }

    fun getChat(userId1: String, userId2: String): LiveData<Chat> {
        if (chat.value == null) {
            if (chatId.isNotEmpty()) {
                loadChat(chatId)
            } else {
                loadChat(userId1, userId2)
            }

        }
        return chat
    }

    fun getChat(chatId: String): LiveData<Chat> {
        if (chat.value == null) {
            if (chatId.isNotEmpty()) {
                loadChat(chatId)
            } else {
                loadChat(currentUser.value!!.userId, userId2)
            }

        }
        return chat
    }

    fun getCurrentUser(): LiveData<User> {
        if (currentUser.value == null) {
            loadCurrentUser()
        }
        return currentUser
    }

    private fun loadChat(chatId: String) {

        if (currentUser.value == null) {
            return
        }

        repository.getChat(chatId)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.toObservable()
            ?.subscribe(object : Observer<Chat> {
                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                    onQuery.value = State.LOADING
                }

                override fun onNext(t: Chat) {
                    chat.value = t
                }

                override fun onError(e: Throwable) {
                    onQuery.value = State.ERROR
                }

                override fun onComplete() {
                    onQuery.value = State.SUCCESS
                }
            })
    }

    private fun loadChat(userId1: String, userId2: String) {
        this.userId2 = userId2
        var chatId = ""

        if (currentUser.value == null) {
            return
        }

        currentUser.value?.chats?.forEach {
            if ((it == "$userId1$userId2" || it == "$userId2$userId1") && it != "$userId1$userId1") {
                chatId = it
            }

        }

        if (chatId == "") {
            currentUser.value?.chats?.forEach {
                if (it == "$userId2$userId2") {
                    chatId = it
                }
            }
        }

        if (chatId != "") {
            repository.getChat(chatId)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.toObservable()
                ?.subscribe(object : Observer<Chat> {
                    override fun onSubscribe(d: Disposable) {
                        disposable.add(d)
                        onQuery.value = State.LOADING
                    }

                    override fun onNext(t: Chat) {
                        chat.value = t
                    }

                    override fun onError(e: Throwable) {
                        onQuery.value = State.ERROR
                    }

                    override fun onComplete() {
                        onQuery.value = State.SUCCESS
                    }
                })

        } else {
            repository.createChat(userId1, userId2)!!
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {
                        disposable.add(d)
                    }

                    override fun onComplete() {
                        loadCurrentUser()

                    }

                    override fun onError(e: Throwable) {
                        onSend.value = State.ERROR
                    }
                })
        }
    }

    private fun loadCurrentUser() {
        repository.getCurrentUser()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.toObservable()
            ?.subscribe(object : Observer<User> {
                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                    onQuery.value = State.LOADING
                }

                override fun onNext(t: User) {
                    currentUser.value = t
                }

                override fun onError(e: Throwable) {
                    onQuery.value = State.ERROR
                }

                override fun onComplete() {
                    onQuery.value = State.SUCCESS

                    if (chatId.isNotEmpty()) {
                        loadChat(chatId)
                    } else {
                        loadChat(currentUser.value!!.userId, userId2)
                    }

                }
            })
    }

    fun uploadImage() {
        repository.uploadImage(image.value!!)!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .toObservable()
            .subscribe(object : Observer<String> {
                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                    onQuery.value = State.LOADING
                }

                override fun onNext(t: String) {
                    sendMessage(message = t)
                }

                override fun onError(e: Throwable) {
                    onQuery.value = State.ERROR
                }

                override fun onComplete() {}
            })
    }


    fun sendMessage(message: String) {
        repository.getCurrentUser()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.toObservable()
            ?.subscribe(object : Observer<User> {
                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                    onSend.value = State.LOADING
                }

                override fun onNext(t: User) {
                    val newMessage = Message(t.userId, t.nickname, message, Date().time)
                    val messageList: List<Message> = chat.value?.messages!!
                    (messageList as ArrayList).add(newMessage)

                    repository.updateChat(chat.value!!)
                        ?.subscribeOn(Schedulers.io())
                        ?.observeOn(AndroidSchedulers.mainThread())
                        ?.subscribe(object : CompletableObserver {
                            override fun onSubscribe(d: Disposable) {
                                disposable.add(d)
                            }

                            override fun onComplete() {
                                onSend.value = State.SUCCESS
                            }

                            override fun onError(e: Throwable) {
                                onSend.value = State.ERROR
                            }
                        })
                }

                override fun onError(e: Throwable) {
                    onSend.value = State.ERROR
                }

                override fun onComplete() {
                    onSend.value = State.SUCCESS
                }
            })
    }

    fun observeQuery(): LiveData<State> {
        return onQuery
    }

    fun observeSend(): LiveData<State> {
        return onSend
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}