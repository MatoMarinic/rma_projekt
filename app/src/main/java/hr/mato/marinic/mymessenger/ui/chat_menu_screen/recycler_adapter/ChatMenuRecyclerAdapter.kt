package hr.mato.marinic.mymessenger.ui.chat_menu_screen.recycler_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import hr.mato.marinic.mymessenger.R
import hr.mato.marinic.mymessenger.databinding.ChatItemMaterialBinding
import hr.mato.marinic.mymessenger.model.Chat

class ChatMenuRecyclerAdapter(
    private val userId: String,
    private val onSelected: (String) -> Unit,

    ) :
    RecyclerView.Adapter<ChatMenuViewHolder>(), Filterable {

    private var chatList: MutableList<Chat> = mutableListOf()
    private var currentUserNickname: String = ""

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ChatMenuViewHolder {
        val layoutInflater = LayoutInflater.from(p0.context)
        val binding: ChatItemMaterialBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.chat_item_material, p0, false)
        return ChatMenuViewHolder(binding)
    }

    override fun getItemCount() = chatList.size

    override fun onBindViewHolder(p0: ChatMenuViewHolder, p1: Int) {
        p0.onBind(
            chatList,
            userId,
            currentUserNickname,
            p1,
            onSelected,

            )
    }

    fun addItems(list: List<Chat>) {
        chatList.clear()
        chatList = list.toMutableList()
        notifyDataSetChanged()
    }

    fun addUserNickname(nickname: String) {
        currentUserNickname = nickname
        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        TODO("Not yet implemented")
    }
}