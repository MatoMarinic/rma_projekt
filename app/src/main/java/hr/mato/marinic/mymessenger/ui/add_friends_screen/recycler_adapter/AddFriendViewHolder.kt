package hr.mato.marinic.mymessenger.ui.add_friends_screen.recycler_adapter

import androidx.recyclerview.widget.RecyclerView
import hr.mato.marinic.mymessenger.databinding.UserItemBinding
import hr.mato.marinic.mymessenger.model.User

class AddFriendViewHolder(private val userItemBinding: UserItemBinding) :
    RecyclerView.ViewHolder(userItemBinding.root) {


    fun onBind(
        usersList: List<User>,
        position: Int,
        onSelected: (String) -> Unit,
    ) {

        itemView.setOnClickListener { onSelected(usersList[position].userId) }
        userItemBinding.user = usersList[position]
        userItemBinding.executePendingBindings()
    }
}