package hr.mato.marinic.mymessenger.ui.chat_screen

import android.R.attr
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import hr.mato.marinic.mymessenger.R
import hr.mato.marinic.mymessenger.databinding.FragmentChatBinding
import hr.mato.marinic.mymessenger.ui.chat_screen.recycler_adapter.MessageRecyclerAdapter
import hr.mato.marinic.mymessenger.utils.*
import hr.mato.marinic.mymessenger.utils.Constants.CHAT_ID_EXTRA
import hr.mato.marinic.mymessenger.utils.Constants.PICK_IMAGE_REQUEST
import hr.mato.marinic.mymessenger.utils.Constants.USER_ID_EXTRA
import hr.mato.marinic.mymessenger.viewmodels.ChatViewModel
import kotlinx.android.synthetic.main.fragment_chat.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.ByteArrayOutputStream


@Suppress("DEPRECATED_IDENTITY_EQUALS")
class ChatFragment : Fragment() {

    private val viewModel by viewModel<ChatViewModel>()
    private lateinit var navController: NavController
    lateinit var userId: String
    var userId2: String = ""
    lateinit var chatId: String
    private val adapter by lazy { MessageRecyclerAdapter() }
    private lateinit var resultLauncher: ActivityResultLauncher<Intent>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        chatId = requireArguments().getString(CHAT_ID_EXTRA) ?: ""

        if (chatId.isEmpty()) {
            userId2 = requireArguments().getString(USER_ID_EXTRA) ?: ""
        }
        viewModel.chatId = chatId
        val binding: FragmentChatBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false)
        binding.chatRecyclerView.adapter = adapter

        return binding.root
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        chatToolbar.navigationIcon = resources.getDrawable(
            R.drawable.ic_baseline_arrow_back_24,
            null
        )
        chatToolbar.setNavigationOnClickListener { navController.popBackStack() }
        sendButton.setOnClickListener { sendMessage() }
        imageButton.setOnClickListener { sendImage() }


        resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == Activity.RESULT_OK) {
                    handleCameraImage(result.data)
                }
            }

        initRecycler()
        setImeAction()
        subscribeObservers()
        observeUser()
        observeChat()
    }

    private fun handleCameraImage(intent: Intent?) {
        val bitmap = intent?.extras?.get("data") as Bitmap
        compressImage(bitmap)

    }

    fun compressImage(bitmap: Bitmap) {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 25, baos)
        val imageData = baos.toByteArray()
        viewModel.setImage(imageData)
        viewModel.uploadImage()
    }

    private fun setImeAction() {
        enteMessageEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                sendMessage()
                true
            } else false
        }
    }

    private fun sendMessage() {
        viewModel.sendMessage(enteMessageEditText.text.trim().toString())
        enteMessageEditText.text.clear()
        hideKeyboard()
    }


    private fun sendImage() {
        onSendImage()
        hideKeyboard()
    }

    private fun initRecycler() {
        chatRecyclerView.layoutManager = LinearLayoutManager(context)

        chatRecyclerView.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (attr.bottom > oldBottom) {
                chatRecyclerView.postDelayed(
                    Runnable
                    {
                        chatRecyclerView.smoothScrollToPosition((adapter.itemCount))
                    }, 100
                )
            }
        }
    }

    private fun getCurrentUserId(): String? {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        return sharedPref?.getString(Constants.USER_ID_SHARED, "")
    }


    private fun observeUser() {
        viewModel.getCurrentUser().removeObservers(this)
        viewModel.getCurrentUser().observe(viewLifecycleOwner, Observer { user ->
            run {
                adapter.setUserId(user.userId)
                observeChat()
            }
        })

    }

    private fun observeChat() {
        if (chatId.isNotEmpty()) {
            viewModel.getChat(chatId).removeObservers(this)
            viewModel.getChat(chatId)
                .observe(viewLifecycleOwner, Observer { chat ->
                    run {
                        adapter.addItems(chat.messages)

                        val nickname = if (chat.nicknames?.size == 1) {
                            chat.nicknames?.first() ?: ""
                        } else {
                            chat.nicknames?.first { it != viewModel.getCurrentUser().value?.nickname }
                                ?: ""
                        }

                        chatToolbar.title = nickname
                    }
                })
        } else {
            viewModel.getChat(getCurrentUserId()!!, userId2).removeObservers(this)
            viewModel.getChat(getCurrentUserId()!!, userId2)
                .observe(viewLifecycleOwner, Observer { chat ->
                    run {
                        adapter.addItems(chat.messages)
                    }
                })
        }
    }


    private fun subscribeObservers() {
        viewModel.observeQuery().observe(viewLifecycleOwner, Observer<State> {
            if (it != null) {
                when (it) {
                    State.LOADING -> show(chatProgress)
                    State.SUCCESS -> hide(chatProgress)
                    State.ERROR -> hide(chatProgress)
                }
            }
        })

        viewModel.observeSend().observe(viewLifecycleOwner, Observer<State> {
            if (it != null) {
                when (it) {
                    State.LOADING -> show(chatProgress)
                    State.SUCCESS -> {
                        observeChat()
                        hide(chatProgress)
                    }
                    State.ERROR -> hide(chatProgress)
                }
            }
        })
    }

    private fun onSendImage(): Boolean {

        SendImageDialog.newInstance(object : SendImageListener {
            override fun onCamera() {
                navigateToCamera()
            }

            override fun onGallery() {
                navigateToGallery()
            }

        }).show(childFragmentManager, "sendImageFragment")
        return true
    }

    private fun navigateToCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        resultLauncher.launch(cameraIntent)
    }

    private fun navigateToGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            val selectedPhotoUri = data.data
            try {
                selectedPhotoUri?.let {
                    if (Build.VERSION.SDK_INT < 28) {
                        val bitmap = MediaStore.Images.Media.getBitmap(
                            requireActivity().contentResolver,
                            selectedPhotoUri
                        )
                        compressImage(bitmap)
                    } else {
                        val source = ImageDecoder.createSource(
                            requireActivity().contentResolver,
                            selectedPhotoUri
                        )
                        val bitmap = ImageDecoder.decodeBitmap(source)
                        compressImage(bitmap)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}