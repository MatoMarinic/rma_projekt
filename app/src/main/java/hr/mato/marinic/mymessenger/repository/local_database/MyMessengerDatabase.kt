package hr.mato.marinic.mymessenger.repository.local_database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import hr.mato.marinic.mymessenger.model.Chat
import hr.mato.marinic.mymessenger.model.User
import hr.mato.marinic.mymessenger.repository.local_database.converters.TypeConverter
import hr.mato.marinic.mymessenger.repository.local_database.dao.ChatDao
import hr.mato.marinic.mymessenger.repository.local_database.dao.UserDao

@Database(entities = [Chat::class, User::class], version = 1, exportSchema = false)
@TypeConverters(TypeConverter::class)
abstract class MyMessengerDatabase : RoomDatabase() {

    abstract fun chatDao(): ChatDao

    abstract fun userDao(): UserDao
}