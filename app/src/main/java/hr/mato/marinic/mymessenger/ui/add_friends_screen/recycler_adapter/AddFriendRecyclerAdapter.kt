package hr.mato.marinic.mymessenger.ui.add_friends_screen.recycler_adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import hr.mato.marinic.mymessenger.R
import hr.mato.marinic.mymessenger.databinding.UserItemBinding
import hr.mato.marinic.mymessenger.model.User

class AddFriendRecyclerAdapter(
    private val userId: String,
    private val onSelected: (String) -> Unit,
) :
    RecyclerView.Adapter<AddFriendViewHolder>(), Filterable {

    private var userList: MutableList<User> = mutableListOf()
    private var filteredUserList: MutableList<User> = mutableListOf()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AddFriendViewHolder {
        val layoutInflater = LayoutInflater.from(p0.context)
        val binding: UserItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.user_item, p0, false)
        return AddFriendViewHolder(binding)
    }

    override fun getItemCount() = filteredUserList.size

    override fun onBindViewHolder(p0: AddFriendViewHolder, p1: Int) {
        p0.onBind(
            filteredUserList,
            p1,
            onSelected,
        )
    }

    fun addItems(list: List<User>) {
        userList.clear()
        userList = list.toMutableList()
        filteredUserList = userList
        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        return exampleFilter
    }

    private val exampleFilter: Filter = object : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            filteredUserList = if (constraint != null && constraint.isNotEmpty()) {
                userList.filter {
                    it.nickname.contains(constraint.toString()) || it.email.contains(constraint.toString())
                }.toMutableList()
            } else {
                userList
            }
            val results = FilterResults()
            results.values = filteredUserList.toList()
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            filteredUserList.clear()
            filteredUserList.addAll(results.values as List<User>)
            notifyDataSetChanged()

        }
    }
}