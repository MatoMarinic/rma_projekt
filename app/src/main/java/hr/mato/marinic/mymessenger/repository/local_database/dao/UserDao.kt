package hr.mato.marinic.mymessenger.repository.local_database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import hr.mato.marinic.mymessenger.model.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNewUser(user: User)
}