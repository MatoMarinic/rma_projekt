package hr.mato.marinic.mymessenger.ui.chat_screen.recycler_adapter


import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import hr.mato.marinic.mymessenger.databinding.MessageItemIncBinding
import hr.mato.marinic.mymessenger.model.Message
import hr.mato.marinic.mymessenger.utils.StringUtils

class IncMessageViewHolder(private val messageItemIncBinding: MessageItemIncBinding) :
    RecyclerView.ViewHolder(messageItemIncBinding.root) {

    fun onBind(messageList: List<Message>, position: Int) {
        if (position != 0 && messageList[position - 1].userId == messageList[position].userId) {
            messageItemIncBinding.chatNicknameInc.visibility = View.GONE
        } else {
            messageItemIncBinding.chatNicknameInc.visibility = View.VISIBLE
        }

        if (messageList[position] == messageList.last()) {
            messageItemIncBinding.chatDateInc.visibility = View.VISIBLE
        } else if (position == 0 && messageList[0].userId == messageList[position + 1].userId) {
            messageItemIncBinding.chatDateInc.visibility = View.GONE
        } else if (position == 0 && messageList[0].userId != messageList[position + 1].userId) {
            messageItemIncBinding.chatDateInc.visibility = View.VISIBLE
        } else if (position < messageList.size && messageList[position].userId != messageList[position + 1].userId) {
            messageItemIncBinding.chatDateInc.visibility = View.VISIBLE
        } else if (messageList[position - 1].userId != messageList[position].userId) {
            messageItemIncBinding.chatDateInc.visibility = View.GONE
        } else if (position < messageList.size && messageList[position].userId != messageList[position + 1].userId) {
            messageItemIncBinding.chatDateInc.visibility = View.VISIBLE
        } else if (messageList[position - 1].userId == messageList[position].userId) {
            messageItemIncBinding.chatDateInc.visibility = View.GONE
        } else {
            messageItemIncBinding.chatDateInc.visibility = View.VISIBLE
        }

        if (messageList[position].content.startsWith("https://firebasestorage.googleapis.com/")) {
            messageItemIncBinding.imageMessageInc.visibility = View.VISIBLE
            messageItemIncBinding.chatMessageInc.visibility = View.GONE
            messageItemIncBinding.imageUrl = messageList[position].content
        } else {
            messageItemIncBinding.imageMessageInc.visibility = View.GONE
            messageItemIncBinding.chatMessageInc.visibility = View.VISIBLE
        }

        messageItemIncBinding.message = messageList[position]
        messageItemIncBinding.stringUtils = StringUtils
        messageItemIncBinding.executePendingBindings()
    }

    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ImageView, url: String?) {
            if (!url.isNullOrEmpty()) {
                Glide.with(view.context).load(url).into(view)
            }
        }
    }
}