package hr.mato.marinic.mymessenger.ui.chat_menu_screen

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import hr.mato.marinic.mymessenger.R
import hr.mato.marinic.mymessenger.ui.chat_menu_screen.recycler_adapter.ChatMenuRecyclerAdapter
import hr.mato.marinic.mymessenger.utils.*
import hr.mato.marinic.mymessenger.utils.Constants.CHAT_ID_EXTRA
import hr.mato.marinic.mymessenger.utils.Constants.IS_LOGGED_IN
import hr.mato.marinic.mymessenger.utils.Constants.LOGIN
import hr.mato.marinic.mymessenger.utils.Constants.USER_ID_SHARED
import hr.mato.marinic.mymessenger.viewmodels.ChatMenuViewModel
import kotlinx.android.synthetic.main.fragment_chat_menu.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChatMenuFragment : Fragment() {

    private val viewModel by viewModel<ChatMenuViewModel>()
    private lateinit var navController: NavController
    private val adapter by lazy {
        ChatMenuRecyclerAdapter(
            getUserId()!!,

            ) { onMessagesSelected(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: hr.mato.marinic.mymessenger.databinding.FragmentChatMenuBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_chat_menu, container, false)
        binding.chatMenuRecyclerView.adapter = adapter
        return binding.root
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()

        fabAddFriend.setOnClickListener { navController.navigate(R.id.action_chatMenuFragment_to_addFriendFragment) }

        chatMenuToolbar.setOnMenuItemClickListener { p0 ->
            when (p0?.itemId) {
                else -> onLogout()
            }
        }

        initRecycler()
        observeData()
        setSearchView()
        subscribeObservers()
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadChats()
    }


    private fun getUserId(): String? {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        return sharedPref?.getString(USER_ID_SHARED, "")
    }


    private fun initRecycler() {
        chatMenuRecyclerView.layoutManager = LinearLayoutManager(context)
    }


    private fun observeData() {
        viewModel.getChats().removeObservers(this)
        viewModel.getChats().observe(viewLifecycleOwner, Observer { taskList ->
            run {
                adapter.addUserNickname(viewModel.getUser().value?.nickname ?: "")
                adapter.addItems(taskList)
            }
        })
    }

    private fun subscribeObservers() {
        viewModel.observeQuery().observe(viewLifecycleOwner, Observer<State> {
            if (it != null) {
                when (it) {
                    State.LOADING -> show(feedListProgress)
                    State.SUCCESS -> hide(feedListProgress)
                    State.ERROR -> hide(feedListProgress)
                }
            }
        })
    }

    private fun setSearchView() {
        chatMenuToolbar.title = "FriendlyMess"
    }

    private fun onMessagesSelected(id: String) {
        goToChatFragment(id)
    }


    private fun goToChatFragment(id: String) {
        navController.navigate(
            R.id.action_chatMenuFragment_to_chatFragment,
            bundleOf(Pair(CHAT_ID_EXTRA, id))
        )
    }

    private fun onLogout(): Boolean {

        LogoutDialog.newInstance(object : BooleanListener {
            override fun onYes() {
                showSnackBar("You have been logged out")
                activity?.getSharedPreferences(LOGIN, 0)?.edit()?.putBoolean(IS_LOGGED_IN, false)
                    ?.apply()
                navController.navigate(R.id.action_chatMenuFragment_to_loginFragment)
            }
        }).show(childFragmentManager, "logoutFragment")
        return true
    }


}