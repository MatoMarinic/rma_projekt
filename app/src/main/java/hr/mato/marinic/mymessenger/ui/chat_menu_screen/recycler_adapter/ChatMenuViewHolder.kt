package hr.mato.marinic.mymessenger.ui.chat_menu_screen.recycler_adapter

import androidx.recyclerview.widget.RecyclerView
import hr.mato.marinic.mymessenger.databinding.ChatItemMaterialBinding
import hr.mato.marinic.mymessenger.model.Chat
import hr.mato.marinic.mymessenger.utils.StringUtils

class ChatMenuViewHolder(private val chatItemMaterialBinding: ChatItemMaterialBinding) :
    RecyclerView.ViewHolder(chatItemMaterialBinding.root) {

    fun onBind(
        chatList: List<Chat>,
        userId: String,
        currentUserNickname: String,
        position: Int,
        onSelected: (String) -> Unit,
    ) {
        itemView.setOnClickListener { onSelected(chatList[position].id) }

        if (chatList[position].messages.size - 1 != -1) {
            val lastMessage =
                chatList[position].messages[chatList[position].messages.size - 1].content

            if (lastMessage.startsWith("https://firebasestorage.googleapis.com/")) {
                chatItemMaterialBinding.lastMessage = "Image was sent."
            } else {
                chatItemMaterialBinding.lastMessage = lastMessage
            }
        }
        val nickname = if (chatList[position].nicknames?.size == 1) {
            chatList[position].nicknames?.first() ?: ""
        } else {
            chatList[position].nicknames?.filter { it != currentUserNickname }?.first() ?: ""
        }

        chatItemMaterialBinding.nickname = nickname

        chatItemMaterialBinding.date =
            chatList[position].messages[chatList[position].messages.size - 1].date

        chatItemMaterialBinding.stringUtils = StringUtils
        chatItemMaterialBinding.executePendingBindings()


    }
}