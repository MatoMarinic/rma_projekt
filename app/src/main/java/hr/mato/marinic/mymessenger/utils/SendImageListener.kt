package hr.mato.marinic.mymessenger.utils

interface SendImageListener {
    fun onCamera()
    fun onGallery()
}