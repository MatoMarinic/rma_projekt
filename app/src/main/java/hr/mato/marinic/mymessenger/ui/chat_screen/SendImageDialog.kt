package hr.mato.marinic.mymessenger.ui.chat_screen

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialogFragment
import hr.mato.marinic.mymessenger.R
import hr.mato.marinic.mymessenger.utils.SendImageListener
import kotlinx.android.synthetic.main.dialog_send_image.*

class SendImageDialog : AppCompatDialogFragment() {
    lateinit var listener: SendImageListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_send_image)
        dialog.setCanceledOnTouchOutside(true)
        dialog.buttonSendCamera.setOnClickListener {
            listener.onCamera()
            dismiss()
        }
        dialog.buttonSendGallery.setOnClickListener {
            listener.onGallery()
            dismiss()
        }
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window!!.attributes = lp
        return dialog
    }

    companion object {
        fun newInstance(listener: SendImageListener) = SendImageDialog().apply {
            this.listener = listener
        }
    }
}