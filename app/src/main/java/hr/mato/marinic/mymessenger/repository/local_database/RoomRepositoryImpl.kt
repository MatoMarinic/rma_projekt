package hr.mato.marinic.mymessenger.repository.local_database

import hr.mato.marinic.mymessenger.model.Chat
import hr.mato.marinic.mymessenger.model.User
import hr.mato.marinic.mymessenger.repository.local_database.dao.ChatDao
import hr.mato.marinic.mymessenger.repository.local_database.dao.UserDao


class RoomRepositoryImpl(db: MyMessengerDatabase) : RoomRepository {

    private val chatDao: ChatDao = db.chatDao()
    private val userDao: UserDao = db.userDao()

    override fun addNewChat(chat: Chat) = chatDao.addNewChat(chat)

    override fun addNewUser(user: User) = userDao.addNewUser(user)

    override fun getSingleChat(id: String) = chatDao.getSingleChat(id)
}